﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppRegister.Models
{
    public class ApplicationSettings
    {
        //to manage access to data in appsetting.json
        public string JWT_Secret { get; set; }
        public string Client_URL { get; set; }
    }
}
