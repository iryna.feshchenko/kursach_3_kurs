﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppRegister.Models
{
    public class Status
    {
        [Key]
        public int idStatus { get; set; }
        public string nameStatus { get; set; }
    }
}
