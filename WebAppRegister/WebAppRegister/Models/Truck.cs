﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppRegister.Models
{
    public class Truck
    {
        [Key]
        public int idTruck { get; set; }
        public string numberCar { get; set; }
    }
}
