﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppRegister.Models
{
    public class Company
    {
        [Key]
        public int id_Company { get; set; }
        public string nameCompany { get; set; }
        public string descCompany { get; set; }
        public string idUser { get; set; }
    }
}
