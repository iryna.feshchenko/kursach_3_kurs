﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppRegister.Models
{
    public class City
    {
        [Key]
        public int idCity { get; set; }
        public string nameCity { get; set; }
    }
}
