﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppRegister.Models
{
    public class Transp
    {
        [Key]
        public int idTraspotations { get; set; }
        public string nameGood { get; set; }
        public string temperatureGood { get; set; }
        public string humidityGood { get; set; }
        public int idCityFirst { get; set; }
        public string firstAdress { get; set; }
        public int idCitySecond { get; set; }
        public string secondAdress { get; set; }
        public int idTruck { get; set; }
        public int idStatus { get; set; }
        public DateTime dateSend { get; set; }
        public DateTime dateArrival { get; set; }
        [Column(TypeName = "nvarchar(150)")]
        public string recieverName { get; set; }
        public string recieverNumber { get; set; }
        public int idCompany { get; set; }
        public string idUser { get; set; }
    }
}
