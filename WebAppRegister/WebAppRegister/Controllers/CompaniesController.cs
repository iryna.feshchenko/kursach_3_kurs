﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebAppRegister.Models;

namespace WebAppRegister.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : Controller
    {
        private readonly AuthenticationContext _context;

        public CompaniesController(AuthenticationContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
        }

        // GET: api/Companies
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Company>>> GetCompanyDetails()
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;
            return await _context.Companies.Where(c=>c.idUser==userId).ToListAsync();
        }


        // GET: api/Companies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Company>> GetCompanyDetail(int id)
        {
            var companyDetail = await _context.Companies.FindAsync(id);

            if (companyDetail == null)
            {
                return NotFound();
            }

            return companyDetail;
        }

        // PUT: api/Companies/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompanyDetail(int id, Company companyDetail)
        {
            if (id != companyDetail.id_Company)
            {
                return BadRequest();
            }

            _context.Entry(companyDetail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Companies
        [HttpPost]
        public async Task<ActionResult<Company>> PostCompanyDetail(Company companyDetail)
        {
            companyDetail.idUser= User.Claims.First(c => c.Type == "UserID").Value;
            _context.Companies.Add(companyDetail);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCompanyDetail", new { id = companyDetail.id_Company }, companyDetail);
        }

        // DELETE: api/Companies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCompanyDetail(int id)
        {
            var companyDetail = await _context.Companies.FindAsync(id);
            if (companyDetail == null)
            {
                return NotFound();
            }

            _context.Companies.Remove(companyDetail);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool UserDetailExists(int id)
        {
            return _context.Companies.Any(e => e.id_Company == id);
        }
    }
}
