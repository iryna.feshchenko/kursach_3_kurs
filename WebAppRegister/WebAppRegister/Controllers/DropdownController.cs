﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAppRegister.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAppRegister.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DropdownController : Controller
    {
        private readonly AuthenticationContext _context;

        public DropdownController(AuthenticationContext context)
        {
            _context = context;
        }


        // GET: api/DropdownController
        [HttpGet("findallCities")]
        public async Task<ActionResult<IEnumerable<City>>> GetTransportationsDetails()
        {
            return await _context.Cities.ToListAsync();
        }

        [HttpGet("findallCompanies")]
        public async Task<ActionResult<IEnumerable<Company>>> GetCompanyDetails()
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;
            return await _context.Companies.Where(c => c.idUser == userId).ToListAsync();
        }

        [HttpGet("findallTrucks")]
        public async Task<ActionResult<IEnumerable<Truck>>> GetTrucksDetails()
        {
            return await _context.Trucks.ToListAsync();
        }

    }
}
