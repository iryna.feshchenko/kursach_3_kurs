﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAppRegister.Models;
using WebAppRegister.ViewModels;

namespace WebAppRegister.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransportationsController : Controller
    {
        private readonly AuthenticationContext _context;

        public TransportationsController(AuthenticationContext context)
        {
            _context = context;
        }





        [HttpGet("findall")]
        public IActionResult findall()
        {
            try
            {

                //string userId = User.Claims.First(c => c.Type == "UserID").Value;
                var data = from city in _context.Cities
                           from truck in _context.Trucks
                           from status in _context.Statuses
                           from company in _context.Companies
                           from transp in _context.Transprs
                           from city2 in _context.Cities
                           where /*transp.idUser == userId &&*/ transp.idCityFirst == city2.idCity
                           && transp.idCitySecond == city.idCity && transp.idTruck == truck.idTruck
                           && transp.idStatus == status.idStatus && transp.idCompany == company.id_Company
                           select new Transportations
                           {
                               idTraspotations=transp.idTraspotations,
                               nameGood=transp.nameGood,
                               temperatureGood=transp.temperatureGood,
                               humidityGood=transp.humidityGood,
                               CityFirst=city2.nameCity,
                               firstAdress=transp.firstAdress,
                               CitySecond=city.nameCity,
                               secondAdress=transp.secondAdress,
                               truck=truck.numberCar,
                               status=status.nameStatus,
                               dateSend=transp.dateSend,
                               dateArrival=transp.dateArrival,
                               recieverName=transp.recieverName,
                               recieverNumber=transp.recieverNumber,
                               company=company.nameCompany,
                               idUser=transp.idUser
                           };
                return Ok(data);


            }
            catch
            {
                return BadRequest();
            }
        }

        // GET: api/Transportations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Transportations>>> GetTransportationsDetails()
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;
            var data = from city in _context.Cities
                       from truck in _context.Trucks
                       from status in _context.Statuses
                       from company in _context.Companies
                       from transp in _context.Transprs
                       from city2 in _context.Cities
                       where transp.idUser == userId && transp.idCityFirst == city2.idCity
                       && transp.idCitySecond == city.idCity && transp.idTruck == truck.idTruck
                       && transp.idStatus == status.idStatus && transp.idCompany == company.id_Company
                       select new Transportations
                       {
                           idTraspotations = transp.idTraspotations,
                           nameGood = transp.nameGood,
                           temperatureGood = transp.temperatureGood,
                           humidityGood = transp.humidityGood,
                           CityFirst = city2.nameCity,
                           firstAdress = transp.firstAdress,
                           CitySecond = city.nameCity,
                           secondAdress = transp.secondAdress,
                           truck = truck.numberCar,
                           status = status.nameStatus,
                           dateSend = transp.dateSend,
                           dateArrival = transp.dateArrival,
                           recieverName = transp.recieverName,
                           recieverNumber = transp.recieverNumber,
                           company = company.nameCompany,
                           idUser = transp.idUser
                       };
            return await data.ToListAsync();
        }


        // GET: api/Transportations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Transp>> GetTransportationsDetail(int id)
        {
            var transpDetail = await _context.Transprs.FindAsync(id);

            if (transpDetail == null)
            {
                return NotFound();
            }

            return transpDetail;
        }

        // PUT: api/Transportations/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTransportationsDetail(int id, Transp transpDetail)
        {
            if (id != transpDetail.idTraspotations)
            {
                return BadRequest();
            }

            _context.Entry(transpDetail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransportationsDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Transportations
        [HttpPost]
        public async Task<ActionResult<Transp>> PostTransportationsDetail(Transp transpDetail)
        {
            transpDetail.idUser = User.Claims.First(c => c.Type == "UserID").Value;
            _context.Transprs.Add(transpDetail);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCompanyDetail", new { id = transpDetail.idTraspotations }, transpDetail);
        }

        // DELETE: api/Transportations/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTransportationsDetail(int id)
        {
            var transpDetail = await _context.Transprs.FindAsync(id);
            if (transpDetail == null)
            {
                return NotFound();
            }

            _context.Transprs.Remove(transpDetail);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool TransportationsDetailExists(int id)
        {
            return _context.Transprs.Any(e => e.idTraspotations == id);
        }

    }
}
