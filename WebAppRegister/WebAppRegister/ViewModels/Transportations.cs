﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppRegister.ViewModels
{
    public class Transportations
    {
        public int idTraspotations { get; set; }
        public string nameGood { get; set; }
        public string temperatureGood { get; set; }
        public string humidityGood { get; set; }
        public string CityFirst { get; set; }
        public string firstAdress { get; set; }
        public string CitySecond { get; set; }
        public string secondAdress { get; set; }
        public string truck { get; set; }
        public string status { get; set; }
        public DateTime dateSend { get; set; }
        public DateTime dateArrival { get; set; }
        public string recieverName { get; set; }
        public string recieverNumber { get; set; }
        public string company { get; set; }
        public string idUser { get; set; }
    }
}
