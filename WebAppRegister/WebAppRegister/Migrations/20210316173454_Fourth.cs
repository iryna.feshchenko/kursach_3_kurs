﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAppRegister.Migrations
{
    public partial class Fourth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transportations");

            migrationBuilder.CreateTable(
                name: "Transprs",
                columns: table => new
                {
                    idTraspotations = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nameGood = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    temperatureGood = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    humidityGood = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    idCityFirst = table.Column<int>(type: "int", nullable: false),
                    firstAdress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    idCitySecond = table.Column<int>(type: "int", nullable: false),
                    secondAdress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    idTruck = table.Column<int>(type: "int", nullable: false),
                    idStatus = table.Column<int>(type: "int", nullable: false),
                    dateSend = table.Column<DateTime>(type: "datetime2", nullable: false),
                    dateArrival = table.Column<DateTime>(type: "datetime2", nullable: false),
                    recieverName = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    recieverNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    idCompany = table.Column<int>(type: "int", nullable: false),
                    idUser = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transprs", x => x.idTraspotations);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transprs");

            migrationBuilder.CreateTable(
                name: "Transportations",
                columns: table => new
                {
                    idTraspotations = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    dateArrival = table.Column<DateTime>(type: "datetime2", nullable: false),
                    dateSend = table.Column<DateTime>(type: "datetime2", nullable: false),
                    firstAdress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    humidityGood = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    idCityFirst = table.Column<int>(type: "int", nullable: false),
                    idCitySecond = table.Column<int>(type: "int", nullable: false),
                    idStatus = table.Column<bool>(type: "bit", nullable: false),
                    idTruck = table.Column<int>(type: "int", nullable: false),
                    nameGood = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    recieverName = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    recieverNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    secondAdress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    temperatureGood = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transportations", x => x.idTraspotations);
                });
        }
    }
}
