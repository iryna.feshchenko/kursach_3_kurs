﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAppRegister.Migrations
{
    public partial class Third : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "idUser",
                table: "Companies",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    idCity = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nameCity = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.idCity);
                });

            migrationBuilder.CreateTable(
                name: "Statuses",
                columns: table => new
                {
                    idStatus = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nameStatus = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Statuses", x => x.idStatus);
                });

            migrationBuilder.CreateTable(
                name: "Transportations",
                columns: table => new
                {
                    idTraspotations = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nameGood = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    temperatureGood = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    humidityGood = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    idCityFirst = table.Column<int>(type: "int", nullable: false),
                    firstAdress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    idCitySecond = table.Column<int>(type: "int", nullable: false),
                    secondAdress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    idTruck = table.Column<int>(type: "int", nullable: false),
                    idStatus = table.Column<bool>(type: "bit", nullable: false),
                    dateSend = table.Column<DateTime>(type: "datetime2", nullable: false),
                    dateArrival = table.Column<DateTime>(type: "datetime2", nullable: false),
                    recieverName = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    recieverNumber = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transportations", x => x.idTraspotations);
                });

            migrationBuilder.CreateTable(
                name: "Trucks",
                columns: table => new
                {
                    idTruck = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    numberCar = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trucks", x => x.idTruck);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "Statuses");

            migrationBuilder.DropTable(
                name: "Transportations");

            migrationBuilder.DropTable(
                name: "Trucks");

            migrationBuilder.AlterColumn<int>(
                name: "idUser",
                table: "Companies",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);
        }
    }
}
