import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { LoginComponent } from './user/login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './auth/auth.guard';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { MainComponent } from './user/main/main.component';
import { CompaniesDetailsComponent } from './home/companies-details/companies-details.component';
import { TransportationDetails } from './shared/transportation-details.model';
import { TransportationsDetailsComponent } from './home/transportations-details/transportations-details.component';

const routes: Routes = [
  {path:'',redirectTo:'/user/main',pathMatch:'full'},
  {
    path: 'user', component: UserComponent,
    children: [
      { path: 'registration', component: RegistrationComponent, },
      { path: 'login', component: LoginComponent },
      {path:'main', component:MainComponent},
    ]
  },
  {path:'home',component:HomeComponent,canActivate:[AuthGuard],
children:[
{path:'companies-details',component:CompaniesDetailsComponent},
{path:'transportation-details',component:TransportationsDetailsComponent}
] },
  {path:'forbidden',component:ForbiddenComponent},
  {path:'adminpanel',component:AdminPanelComponent,canActivate:[AuthGuard],data :{permittedRoles:['Admin']}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }