import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { CityService } from 'src/app/shared/city.service';
import { Trans } from 'src/app/shared/trans.model';
import { TransportationDetails } from 'src/app/shared/transportation-details.model';
import { TransportationDetailsService } from 'src/app/shared/transportation-details.service';

@Component({
  selector: 'app-transportations-details',
  templateUrl: './transportations-details.component.html',
  styles: [
  ]
})
export class TransportationsDetailsComponent implements OnInit {

  closeResult = '';
  showModalBox: boolean = false;
  results:any;

  constructor(public service: TransportationDetailsService,
    private modalService: NgbModal,
    public cityService:CityService,
    private toastr:ToastrService) { }

  ngOnInit(): void {
    this.service.refreshList();
    this.cityService.getCityList();
    this.cityService.getCompanyList();
    this.cityService.getTrucksList();
  }

  populateForm(selectedRecord: TransportationDetails) {
    this.service.formData = Object.assign({}, selectedRecord);
  }




  populateToEdit(id:number){
    this.service.getOneTransportDetail(id).subscribe
    (
      result => {
        if (result == [])
          console.log('true');
        else {
          this.service.getOneTransportDetail(id).subscribe((data:any)=>
          {
            this.results = data;
            this.results=Array.of(this.results);
            console.log(result)
          });
        }
      }
    );

    console.log(id);
    if(0){
      // Dont open the modal
      this.showModalBox = false;
    } else {
       // Open the modal
       this.showModalBox = true;
    }
  }



  onDelete(id: number) {
    if (confirm('Are you sure to delete this record?')) {
      this.service.deletePaymentDetail(id)
        .subscribe(
          res => {
            this.service.refreshList();
            this.toastr.error("Deleted succesfully",'Payment detail');
          },
          err => { console.log(err) }
        )
    }
  }

  open(content: any) { 
    this.modalService.open(content, 
   {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => { 
      this.closeResult = `Closed with: ${result}`; 
    }, (reason) => { 
      this.closeResult =  
         `Dismissed ${this.getDismissReason(reason)}`; 
    }); 
  } 
  private getDismissReason(reason: any): string { 
    if (reason === ModalDismissReasons.ESC) { 
      return 'by pressing ESC'; 
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) { 
      return 'by clicking on a backdrop'; 
    } else { 
      return `with: ${reason}`; 
    } 
  }




  onSubmit(form:NgForm){
    console.log(this.service.formData2.dateArrival);
    console.log(this.service.formData2.idTraspotations);
      this.insertRecord(form);
  }

  insertRecord(form:NgForm){
    this.service.postPaymentDetail().subscribe(
      res=>{
          this.resetForm(form);
          this.service.refreshList();
      },
      err=>{console.log(err);
      this.resetForm(form);
      this.service.refreshList();
      this.toastr.show("Inserted succesfully",'Payment detail');}
  );
  }
   //reset form after add data on succes
   resetForm(form:NgForm){
    form.form.reset();
    this.service.formData2 = new Trans();
  }


  updateRecord(selectedRecord:Trans){
    this.service.formData2 = Object.assign({},selectedRecord);
    this.service.putPaymentDetail().subscribe(
      res=>{
          this.service.refreshList();
          this.toastr.success("Updated succesfully",'Payment detail');
      },
      err=>{console.log(err);}
  );
  }

}
