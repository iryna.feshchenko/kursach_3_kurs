import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { CompanyDetailService } from 'src/app/shared/company-detail.service';

@Component({
  selector: 'app-companies-details',
  templateUrl: './companies-details.component.html',
  styleUrls: ['./companies-details.component.css']
})
export class CompaniesDetailsComponent implements OnInit {
  fg?:FormGroup;
  companiesDetailsForms: FormArray = this.fb.array([]);
  results:any;

  constructor(private fb: FormBuilder, public userService:CompanyDetailService) { }

  ngOnInit(): void {
    this.userService.getBankAccountList().subscribe(
      res => {
        if (res == [])
          this.addUserAccountForms();
        else {
          //generate formarray as per the data received from BankAccont table
          (res as []).forEach((userAccount: any) => {
            this.companiesDetailsForms.push(this.fb.group({
              id_Company: [userAccount.id_Company],
              nameCompany: [userAccount.nameCompany],
              descCompany: [userAccount.descCompany],
              idUser:[userAccount.idUser]
            }));
          });
        }
      }
    );

  }

  addUserAccountForms() {
    this.companiesDetailsForms.push(this.fb.group({
      id_Company: [0],
      nameCompany: [''],
      descCompany: [''],
      idUser:['']
    }));
  }

  recordSubmit(fg: FormGroup) {
    if (fg.value.id_Company == 0)
      this.userService.postBankAccount(fg.value).subscribe(
        (res: any) => {
          fg.patchValue({ id_Company: res.id_Company });
          console.log("add");
        });
    else
      this.userService.putBankAccount(fg.value).subscribe(
        (res: any) => {
          console.log("update");
        });
  }

  fileChangeEvent(fg: FormGroup) {
    if (fg.value.id_Company == 0)
      this.userService.postBankAccount(fg.value).subscribe(
        (res: any) => {
          fg.patchValue({ id_Company: res.id_Company });
          console.log("add");
        });
    else
      this.userService.putBankAccount(fg.value).subscribe(
        (res: any) => {
          console.log("update");
        });
 };
 
  onDelete(id:number, i:number) {
    if (id == 0)
      this.companiesDetailsForms.removeAt(i);
    else if (confirm('Are you sure to delete this record ?'))
      this.userService.deleteBankAccount(id).subscribe(
        res => {
          this.companiesDetailsForms.removeAt(i);
        });
  }

}
