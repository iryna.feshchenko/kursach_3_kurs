import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TransportationsDetailsComponent } from '../home/transportations-details/transportations-details.component';
import { Trans } from './trans.model';
import { TransportationDetails } from './transportation-details.model';

@Injectable({
  providedIn: 'root'
})
export class TransportationDetailsService {

  constructor(private http:HttpClient) { }

  formData: TransportationDetails = new TransportationDetails();
  formData2: Trans = new Trans();
  list: TransportationDetails[];


  postPaymentDetail() {
    return this.http.post(environment.apiBaseURI1, this.formData2);
  }

  putPaymentDetail() {
    return this.http.put(`${environment.apiBaseURI1}/${this.formData2.idTraspotations}`, this.formData2);
  }

  deletePaymentDetail(id: number) {
    return this.http.delete(`${environment.apiBaseURI1}/${id}`);
  }


  getOneTransportDetail(id:number){
    return this.http.get(`${environment.apiBaseURI1}/${id}`);
  }

  
  refreshList() {
    this.http.get(environment.apiBaseURI1)
      .toPromise()
      .then(res =>this.list = res as TransportationDetails[]);
  }

}
