export class Trans {
    idTraspotations:number;
    nameGood:string;
    temperatureGood:string;
    humidityGood:string;
    idCityFirst:number;
    firstAdress:string;
    idCitySecond:number;
    secondAdress:string;
    idTruck:number;
    idStatus:number;
    dateSend:Date;
    dateArrival:Date;
    recieverName:string;
    recieverNumber:string;
    idCompany:number;
    idUser :string;
}
