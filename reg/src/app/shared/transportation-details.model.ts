export class TransportationDetails {
    idTraspotations:number;
    nameGood:string;
    temperatureGood:string;
    humidityGood:string;
    cityFirst:number;
    firstAdress:string;
    citySecond:number;
    secondAdress:string;
    truck:number;
    status:number;
    dateSend:Date;
    dateArrival:Date;
    recieverName:string;
    recieverNumber:string;
    company:number;
    idUser :string;
}
