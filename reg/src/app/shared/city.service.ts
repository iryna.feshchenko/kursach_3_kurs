import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CompaniesDetailsComponent } from '../home/companies-details/companies-details.component';
import { City } from './city.model';
import { Companies } from './companies.model';
import { Trucks } from './trucks.model';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  list: City[];
  listcomp:Companies[];
  listtrucks:Trucks[];

  constructor(private http:HttpClient) { }

    
  getCityList(){
    this.http.get(environment.apiBaseURI + '/Dropdown/findallCities')
      .toPromise()
      .then(res =>this.list = res as City[]);
  }
  getCompanyList(){
    this.http.get(environment.apiBaseURI + '/Dropdown/findallCompanies')
      .toPromise()
      .then(res =>this.listcomp = res as Companies[]);
  }
  getTrucksList(){
    this.http.get(environment.apiBaseURI + '/Dropdown/findallTrucks')
      .toPromise()
      .then(res =>this.listtrucks = res as Trucks[]);
  }
}
