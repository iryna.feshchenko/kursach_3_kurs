import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompanyDetailService {

  constructor(private http:HttpClient) { }

  postBankAccount(formData: any) {
    return this.http.post(environment.apiBaseURI + '/Companies', formData);
  }

  putBankAccount(formData: { id_Company: number;nameCompany:string;descCompany:string }) {
    return this.http.put(environment.apiBaseURI + '/Companies/' + formData.id_Company, formData);
  }

  deleteBankAccount(id: number) {
    return this.http.delete(environment.apiBaseURI + '/Companies/' + id);
  }

  getBankAccountList() {
    return this.http.get(environment.apiBaseURI + '/Companies');
  }

}
